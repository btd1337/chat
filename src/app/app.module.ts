import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NbThemeModule } from '@nebular/theme';
import { HttpClientModule } from '@angular/common/http';
import { NbAuthModule, NbPasswordAuthStrategy, NbAuthJWTToken } from '@nebular/auth';
import { environment } from 'src/environments/environment';
import { AuthRoutes } from './enums/auth-routes.enum';
import { AuthStrategies } from './enums/auth-strategies.enum';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { AuthModule } from './auth/auth.module';
import { PagesModule } from './pages/pages.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    // Core
    AppRoutingModule,
    BrowserModule,
    HttpClientModule,

    // 3rd libraries
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: AuthStrategies.DEFAULT,
          baseEndpoint: `${environment.serverUrl}/${environment.apiVersion}`,
          login: {
            endpoint: `/auth/${AuthRoutes.LOG_IN}`,
            method: 'post',
            requireValidToken: true,
          },
          register: {
            endpoint: `/auth/${AuthRoutes.REGISTER}`,
            method: 'post',
            requireValidToken: false,
            redirect: {
              success: `/auth/${AuthRoutes.LOG_IN}`
            }
          },
          logout: {
            endpoint: `/auth/${AuthRoutes.LOG_OUT}`,
            method: 'post',
          },
          requestPass: {
            endpoint: `/auth/${AuthRoutes.REQUEST_PASSWORD}`,
            method: 'post'
          },
          resetPass: {
            endpoint: `/auth/${AuthRoutes.RESET_PASSWORD}`,
          },
          token: {
            class: NbAuthJWTToken,
            key: 'token'
          },
        }),
      ],
      forms: {
        login: {
          strategy: AuthStrategies.DEFAULT,
          redirectDelay: 1500
        },
        register: {
          strategy: AuthStrategies.DEFAULT
        },
        logout: {
          strategy: AuthStrategies.DEFAULT,
          redirectDelay: 1500
        }
      },
    }),
    NbEvaIconsModule,
    NbThemeModule.forRoot(),

    // App
    AuthModule,
    PagesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
