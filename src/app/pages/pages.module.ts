import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeChatComponent } from './home-chat/home-chat.component';

export const routes: Routes = [
  {
    path: '', component: HomeChatComponent 
  },
]

@NgModule({
  declarations: [HomeChatComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PagesModule { }
