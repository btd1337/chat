import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './auth.guard';
import { RouterModule, Routes } from '@angular/router';
import { NbAuthComponent, NbLoginComponent, NbLogoutComponent, NbRegisterComponent, NbRequestPasswordComponent, NbResetPasswordComponent } from '@nebular/auth';
import { AuthRoutes } from '../enums/auth-routes.enum';

export const routes: Routes = [
  {
    path: '',
    component: NbAuthComponent,
    children: [
      {
        path: '',
        component: NbLoginComponent,
      },
      {
        path: AuthRoutes.LOG_IN,
        component: NbLoginComponent,
      },
      {
        path: AuthRoutes.REGISTER,
        component: NbRegisterComponent,
      },
      {
        path: AuthRoutes.LOG_OUT,
        component: NbLogoutComponent,
      },
      {
        path: AuthRoutes.REQUEST_PASSWORD,
        component: NbRequestPasswordComponent,
      },
      {
        path: AuthRoutes.RESET_PASSWORD,
        component: NbResetPasswordComponent,
      },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  providers: [
    AuthGuard
  ]
})
export class AuthModule { }
