export enum AuthRoutes {
    LOG_IN = 'signin',
    REGISTER = 'register',
    LOG_OUT = 'logout',
    REQUEST_PASSWORD = 'forgot-password',
    RESET_PASSWORD = 'reset-pass',
}